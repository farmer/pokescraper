import os
from subprocess import call
import threading

def scrapeList(pokemonNames):
    url = "http://www.pokemon.com/uk/pokedex/"
    for pokemonName in pokemonNames:
        call(["wget", url + pokemonName, "--directory-prefix=html/"])

def parseHtml(filenames):
    for filename in filenames:
        file = open("html/" + filename)
        for line in file:
            splitLine = line.split("\"")
            for segment in splitLine:
                if "pokemon.com/assets/cms2/img/cards/web/" in segment:
                    call(["wget", segment, "--directory-prefix=png/"])

pokemonNames = []
pokemonFile = open("pokemon.txt")
for line in pokemonFile:
    pokemonNames.append(line[:-1])

threads = []

workSize = 100
index = 0
# while index < len(pokemonNames):
#     t = threading.Thread(target=scrapeList, args=(pokemonNames[index:index+workSize],))
#     threads.append(t)
#     t.start()
#     index += workSize

for t in threads:
    t.join()
threads = []

filenames = os.listdir("html")
while index < len(filenames):
    t = threading.Thread(target=parseHtml, args=(filenames[index:index+workSize],))
    threads.append(t)
    t.start()
    index += workSize

for t in threads:
    t.join()
threads = []
